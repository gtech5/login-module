package com.rezza.login.controller;

import lombok.Data;

@Data
public class RequestModel {
    private String phone;
    private String email;
}
