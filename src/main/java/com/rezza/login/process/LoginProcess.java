package com.rezza.login.process;

import com.rezza.login.controller.RequestModel;
import com.rezza.login.db.mapper.TbMemberMapper;
import com.rezza.login.db.model.TbMember;
import com.rezza.login.db.model.TbMemberExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class LoginProcess {
//    @Autowired
    private TbMemberMapper memberMapper;

    public TbMember login(RequestModel requestModel) {
        TbMemberExample memberExample = new TbMemberExample();
        memberExample.createCriteria().andTbmMobilePhoneEqualTo(requestModel.getPhone())
                .andTbmEmailEqualTo(requestModel.getEmail());
        return memberMapper.selectByExample(memberExample).get(0);
    }
}